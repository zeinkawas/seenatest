package com.chat.seenatest.service

import com.chat.seenatest.model.BaseResponse
import com.chat.seenatest.model.DataModel
import retrofit2.http.*

interface RetrofitService {

    @GET("books.json")
    suspend fun getData(): BaseResponse<DataModel>

}