package com.chat.seenatest.service


import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RetrofitClient @Inject
constructor(private val retrofitService: RetrofitService) {

    suspend fun getData() = withContext(IO) {
        retrofitService.getData()
    }

}