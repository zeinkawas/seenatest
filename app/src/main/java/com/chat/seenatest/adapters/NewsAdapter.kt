package com.chat.seenatest.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.chat.seenatest.R
import com.chat.seenatest.databinding.ListItemNewsBinding
import com.chat.seenatest.model.DataModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class NewsAdapter : RecyclerView.Adapter<NewsAdapter.NewsHolder>() {
    var mData: ArrayList<DataModel>? = null
    var onNewsClick: OnNewsClick? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsHolder {
        val binding: ListItemNewsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_news,
            parent,
            false
        )

        return NewsHolder(binding)
    }


    override fun onBindViewHolder(holder: NewsHolder, position: Int) {
        if (mData?.size!! > position)
            holder.onBind(mData!![position])
    }


    override fun getItemCount(): Int {
        return if (mData == null) {
            0
        } else {
            mData?.size!!
        }
    }


    inner class NewsHolder(private val itemList: ListItemNewsBinding) : RecyclerView.ViewHolder(itemList.root) {

        fun onBind(item: DataModel) {
            itemList.data = item
        }


        init {
            itemList.root.setOnClickListener {
                onNewsClick?.onClick(mData!![layoutPosition])
            }
        }
    }


    interface OnNewsClick {
        fun onClick(data: DataModel)
    }


}