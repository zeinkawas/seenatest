package com.chat.seenatest.utlis

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.android.material.imageview.ShapeableImageView
import java.text.SimpleDateFormat
import java.util.*


@BindingAdapter("loadImage")
fun loadImage(imageView: ShapeableImageView, url: String) {
    Glide.with(imageView).load(url).skipMemoryCache(false).into(imageView)
}

@BindingAdapter("setDateText")
fun setDateText(textView: TextView, text: String) {

    val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    val dateObj: Date = dateFormat.parse(text)!!

    val finalDate = dateFormat.format(dateObj)
    textView.text = finalDate
}