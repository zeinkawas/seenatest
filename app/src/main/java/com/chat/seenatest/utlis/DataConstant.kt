package com.chat.seenatest.utlis


const val BASE_URL = "https://api.nytimes.com/svc/topstories/v2/"

const val DATA_TITLE = "DATA_TITLE"
const val DATA_PUBLISHED_BY = "DATA_PUBLISHED_BY"
const val DATA_DATE = "DATA_DATE"
const val DATA_IMAGE = "DATA_IMAGE"
const val DATA_RATING = "DATA_RATING"
const val DATA_SUMMARY = "DATA_SUMMARY"
