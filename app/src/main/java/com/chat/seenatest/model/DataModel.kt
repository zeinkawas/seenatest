package com.chat.seenatest.model

data class DataModel(
    val section: String,
    val subsection: String,
    val title: String,
    val abstract: String,
    val url: String,
    val uri: String,
    val byline: String,
    val item_type: String,
    val updated_date: String,
    val created_date: String,
    val published_date: String,
    val des_facet: ArrayList<String>,
    val org_facet: ArrayList<String>,
    val per_facet: ArrayList<String>,
    val multimedia: ArrayList<MultimediaModel>,
    val short_url: String,
)

data class MultimediaModel(
    val url: String,
    val format: String,
    val height: Int,
    val width: Int,
    val subtype: String,
    val type: String,
)