package com.chat.seenatest.model

data class BaseResponse<T>(
    val status: String,
    val copyright: String,
    val section: String,
    val last_updated: String,
    val num_results: Int,
    val results: ArrayList<T>,
)