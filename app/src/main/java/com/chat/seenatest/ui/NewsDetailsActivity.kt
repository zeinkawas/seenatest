package com.chat.seenatest.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.chat.seenatest.R
import com.chat.seenatest.databinding.ActivityNewsDetailsBinding
import com.chat.seenatest.utlis.*

class NewsDetailsActivity : AppCompatActivity() {

    lateinit var binding : ActivityNewsDetailsBinding
    private var title = ""
    private var publishedDate = ""
    private var section = ""
    private var subsection = ""
    private var url = ""
    private var itemType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_news_details)
        receiveData()
        setupListener()

    }

    private fun setupListener() {
        binding.ivPicture.setOnClickListener {
            val intent=Intent(this@NewsDetailsActivity,PreviewImageActivity::class.java)
            intent.putExtra("path", url)
            startActivity(intent)
        }
    }

    private fun receiveData() {
        if (intent.hasExtra(DATA_TITLE)) {
            title = intent.getStringExtra(DATA_TITLE)!!
            subsection = intent.getStringExtra(DATA_SUMMARY)!!
            itemType = intent.getStringExtra(DATA_RATING)!!
            publishedDate = intent.getStringExtra(DATA_DATE)!!
            section = intent.getStringExtra(DATA_PUBLISHED_BY)!!
            url = intent.getStringExtra(DATA_IMAGE)!!


            binding.tvTitle.text = title
            binding.tvDate.text = publishedDate
            binding.tvRating.text = itemType
            binding.tvSummary.text = subsection
            binding.tvPublishedBy.text = section
            Glide.with(this).load(url).into(binding.ivPicture)

        }
    }
}