package com.chat.seenatest.ui

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.chat.seenatest.R
import com.chat.seenatest.adapters.NewsAdapter
import com.chat.seenatest.databinding.ActivityMainBinding
import com.chat.seenatest.model.BaseResponse
import com.chat.seenatest.model.DataModel
import com.chat.seenatest.utlis.*
import com.chat.seenatest.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {


    private lateinit var binding: ActivityMainBinding
    private val mAdapter: NewsAdapter by lazy { NewsAdapter() }
    private val viewModel: HomeViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        init()
        viewModel.getData()
        observers()
    }

    private fun init() {
        binding.rvList.layoutManager = LinearLayoutManager(this)
        binding.rvList.adapter = mAdapter

        mAdapter.onNewsClick = object : NewsAdapter.OnNewsClick {
            override fun onClick(data: DataModel) {
                val intent = Intent(this@MainActivity, NewsDetailsActivity::class.java)
                intent.putExtra(DATA_TITLE, data.title)
                intent.putExtra(DATA_SUMMARY, data.subsection)
                intent.putExtra(DATA_DATE, data.published_date)
                intent.putExtra(DATA_IMAGE, if (data.multimedia[0].url == "") "" else data.multimedia[0].url)
                intent.putExtra(DATA_RATING, data.item_type)
                intent.putExtra(DATA_PUBLISHED_BY, data.section)
                startActivity(intent)
            }

        }

    }

    @SuppressLint("NotifyDataSetChanged")
    private fun observers() {
        viewModel.data.observe(this) {
            when (it.status) {
                NetworkState.Status.RUNNING -> {
                    ProgressLoading.show(this)
                }
                NetworkState.Status.SUCCESS -> {
                    ProgressLoading.dismiss()
                    val data = it.data as BaseResponse<DataModel>
                    mAdapter.mData=data.results
                    mAdapter.notifyDataSetChanged()
                }
                else -> {
                    ProgressLoading.dismiss()
                    Toast.makeText(this, "", Toast.LENGTH_LONG).show()
                }
            }


        }
    }

}