package com.chat.seenatest.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chat.seenatest.service.RetrofitClient
import com.chat.seenatest.utlis.NetworkState
import com.chat.seenatest.utlis.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel
@Inject
constructor(private val retrofitClient: RetrofitClient) : ViewModel() {

    private var _data = SingleLiveEvent<NetworkState>()
    val data: SingleLiveEvent<NetworkState> get() = _data
    fun getData() {
        _data.postValue(NetworkState.LOADING)
        viewModelScope.launch(IO) {
            try {
                val data = retrofitClient.getData()
                if (data.status == "OK") {
                    _data.postValue(NetworkState.getLoaded(data))
                } else {
                    _data.postValue(NetworkState.getErrorMessage(data.status))
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }


    }

}